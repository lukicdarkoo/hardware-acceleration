# Hardware Acceleration

## Documentation
https://www.overleaf.com/read/rmhfwjgbdfks

## Quartus Shortcuts
```
es-modelsim() {
  LD_LIBRARY_PATH=/home/lukic/freetype-2.4.7/lib:$LD_LIBRARY_PATH /home/lukic/intelFPGA_lite/18.1/modelsim_ase/linuxaloem/vsim
}

es-jtagfix() {
  sudo killall jtagd
  sudo /home/lukic/intelFPGA_lite/18.1/quartus/bin/jtagconfig
}

es-quartus() {
  /home/lukic/intelFPGA_lite/18.1/quartus/bin/quartus
}

es-eclipse() {
  /home/lukic/intelFPGA_lite/18.1/nios2eds/bin/eclipse-nios2
}
```